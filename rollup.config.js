import ts from "rollup-plugin-typescript2";

const pkg = require("./package.json");

export default {
    input: "src/index.ts",

    plugins: [
        ts({ verbosity: 2, abortOnError: false })
    ],
    sourceMap: true,
    output: [
        {
            format: "umd",
            name: "feng",
            file: pkg.main,
        },
        {
            format: "es",
            file: pkg.module,
        }
    ],
};