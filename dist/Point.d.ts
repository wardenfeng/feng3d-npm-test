/**
 * Point 对象表示二维坐标系统中的某个位置，其中 x 表示水平轴，y 表示垂直轴。
 */
export declare class Point {
    /**
     * 创建一个 egret.Point 对象.若不传入任何参数，将会创建一个位于（0，0）位置的点。
     * @param x 该对象的x属性值，默认为0
     * @param y 该对象的y属性值，默认为0
     */
    constructor(x?: number, y?: number);
    /**
     * 该点的水平坐标。
     * @default 0
     */
    x: number;
    /**
     * 该点的垂直坐标。
     * @default 0
     */
    y: number;
    /**
     * 从 (0,0) 到此点的线段长度。
     */
    readonly length: number;
    /**
     * 将 Point 的成员设置为指定值
     * @param x 该对象的x属性值
     * @param y 该对象的y属性值
     */
    setTo(x: number, y: number): Point;
    /**
     * 克隆点对象
     */
    clone(): Point;
    /**
     * 确定两个点是否相同。如果两个点具有相同的 x 和 y 值，则它们是相同的点。
     * @param toCompare 要比较的点。
     * @returns 如果该对象与此 Point 对象相同，则为 true 值，如果不相同，则为 false。
     */
    equals(toCompare: Point): boolean;
    /**
     * 返回 pt1 和 pt2 之间的距离。
     * @param p1 第一个点
     * @param p2 第二个点
     * @returns 第一个点和第二个点之间的距离。
     */
    static distance(p1: Point, p2: Point): number;
    /**
     * 将源 Point 对象中的所有点数据复制到调用方 Point 对象中。
     * @param sourcePoint 要从中复制数据的 Point 对象。
     */
    copyFrom(sourcePoint: Point): void;
    /**
     * 将另一个点的坐标添加到此点的坐标以创建一个新点。
     * @param v 要添加的点。
     * @returns 新点。
     */
    add(v: Point): Point;
    /**
     * 确定两个指定点之间的点。
     * 参数 f 确定新的内插点相对于参数 pt1 和 pt2 指定的两个端点所处的位置。参数 f 的值越接近 1.0，则内插点就越接近第一个点（参数 pt1）。参数 f 的值越接近 0，则内插点就越接近第二个点（参数 pt2）。
     * @param pt1 第一个点。
     * @param pt2 第二个点。
     * @param f 两个点之间的内插级别。表示新点将位于 pt1 和 pt2 连成的直线上的什么位置。如果 f=1，则返回 pt1；如果 f=0，则返回 pt2。
     * @returns 新的内插点。
     */
    static interpolate(pt1: Point, pt2: Point, f: number): Point;
    /**
     * 将 (0,0) 和当前点之间的线段缩放为设定的长度。
     * @param thickness 缩放值。例如，如果当前点为 (0,5) 并且您将它规范化为 1，则返回的点位于 (0,1) 处。
     */
    normalize(thickness: number): void;
    /**
     * 按指定量偏移 Point 对象。dx 的值将添加到 x 的原始值中以创建新的 x 值。dy 的值将添加到 y 的原始值中以创建新的 y 值。
     * @param dx 水平坐标 x 的偏移量。
     * @param dy 水平坐标 y 的偏移量。
     */
    offset(dx: number, dy: number): void;
    /**
     * 将一对极坐标转换为笛卡尔点坐标。
     * @param len 极坐标对的长度。
     * @param angle 极坐标对的角度（以弧度表示）。
     */
    static polar(len: number, angle: number): Point;
    /**
     * 从此点的坐标中减去另一个点的坐标以创建一个新点。
     * @param v 要减去的点。
     * @returns 新点。
     */
    subtract(v: Point): Point;
    /**
     * 返回包含 x 和 y 坐标的值的字符串。该字符串的格式为 "(x=x, y=y)"，因此为点 23,17 调用 toString() 方法将返回 "(x=23, y=17)"。
     * @returns 坐标的字符串表示形式。
     */
    toString(): string;
    /**
     * 返回包含 x 和 y 坐标值的数组
     */
    toArray(): number[];
    private xchange(propertyKey, oldValue, newValue);
}
