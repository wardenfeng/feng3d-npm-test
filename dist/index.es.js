/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */







function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

/**
 * 观察装饰器，观察被装饰属性的变化
 *
 * *对使用watch修饰的属性赋值比未使用的性能差距100倍左右*
 * @param onChange 属性变化回调
 */
function watch(onChange) {
    return function (target, propertyKey) {
        console.assert(target[onChange], "\u5BF9\u8C61 " + target + " \u4E2D\u672A\u627E\u5230\u65B9\u6CD5 " + onChange);
        var key = "_" + propertyKey;
        Object.defineProperty(target, propertyKey, {
            get: function () {
                return this[key];
            },
            set: function (value) {
                if (this[key] === value) {
                    return;
                }
                var oldValue = this[key];
                var newValue = this[key] = value;
                target[onChange].apply(this, [propertyKey, oldValue, newValue]);
            },
            enumerable: true,
            configurable: true
        });
    };
}

var DEG_TO_RAD = Math.PI / 180;
/**
 * Point 对象表示二维坐标系统中的某个位置，其中 x 表示水平轴，y 表示垂直轴。
 */
var Point = /** @class */ (function () {
    /**
     * 创建一个 egret.Point 对象.若不传入任何参数，将会创建一个位于（0，0）位置的点。
     * @param x 该对象的x属性值，默认为0
     * @param y 该对象的y属性值，默认为0
     */
    function Point(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.x = x;
        this.y = y;
    }
    Object.defineProperty(Point.prototype, "length", {
        /**
         * 从 (0,0) 到此点的线段长度。
         */
        get: function () {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * 将 Point 的成员设置为指定值
     * @param x 该对象的x属性值
     * @param y 该对象的y属性值
     */
    Point.prototype.setTo = function (x, y) {
        this.x = x;
        this.y = y;
        return this;
    };
    /**
     * 克隆点对象
     */
    Point.prototype.clone = function () {
        return new Point(this.x, this.y);
    };
    /**
     * 确定两个点是否相同。如果两个点具有相同的 x 和 y 值，则它们是相同的点。
     * @param toCompare 要比较的点。
     * @returns 如果该对象与此 Point 对象相同，则为 true 值，如果不相同，则为 false。
     */
    Point.prototype.equals = function (toCompare) {
        return this.x == toCompare.x && this.y == toCompare.y;
    };
    /**
     * 返回 pt1 和 pt2 之间的距离。
     * @param p1 第一个点
     * @param p2 第二个点
     * @returns 第一个点和第二个点之间的距离。
     */
    Point.distance = function (p1, p2) {
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    };
    /**
     * 将源 Point 对象中的所有点数据复制到调用方 Point 对象中。
     * @param sourcePoint 要从中复制数据的 Point 对象。
     */
    Point.prototype.copyFrom = function (sourcePoint) {
        this.x = sourcePoint.x;
        this.y = sourcePoint.y;
    };
    /**
     * 将另一个点的坐标添加到此点的坐标以创建一个新点。
     * @param v 要添加的点。
     * @returns 新点。
     */
    Point.prototype.add = function (v) {
        return new Point(this.x + v.x, this.y + v.y);
    };
    /**
     * 确定两个指定点之间的点。
     * 参数 f 确定新的内插点相对于参数 pt1 和 pt2 指定的两个端点所处的位置。参数 f 的值越接近 1.0，则内插点就越接近第一个点（参数 pt1）。参数 f 的值越接近 0，则内插点就越接近第二个点（参数 pt2）。
     * @param pt1 第一个点。
     * @param pt2 第二个点。
     * @param f 两个点之间的内插级别。表示新点将位于 pt1 和 pt2 连成的直线上的什么位置。如果 f=1，则返回 pt1；如果 f=0，则返回 pt2。
     * @returns 新的内插点。
     */
    Point.interpolate = function (pt1, pt2, f) {
        var f1 = 1 - f;
        return new Point(pt1.x * f + pt2.x * f1, pt1.y * f + pt2.y * f1);
    };
    /**
     * 将 (0,0) 和当前点之间的线段缩放为设定的长度。
     * @param thickness 缩放值。例如，如果当前点为 (0,5) 并且您将它规范化为 1，则返回的点位于 (0,1) 处。
     */
    Point.prototype.normalize = function (thickness) {
        if (this.x != 0 || this.y != 0) {
            var relativeThickness = thickness / this.length;
            this.x *= relativeThickness;
            this.y *= relativeThickness;
        }
    };
    /**
     * 按指定量偏移 Point 对象。dx 的值将添加到 x 的原始值中以创建新的 x 值。dy 的值将添加到 y 的原始值中以创建新的 y 值。
     * @param dx 水平坐标 x 的偏移量。
     * @param dy 水平坐标 y 的偏移量。
     */
    Point.prototype.offset = function (dx, dy) {
        this.x += dx;
        this.y += dy;
    };
    /**
     * 将一对极坐标转换为笛卡尔点坐标。
     * @param len 极坐标对的长度。
     * @param angle 极坐标对的角度（以弧度表示）。
     */
    Point.polar = function (len, angle) {
        return new Point(len * Math.cos(angle / DEG_TO_RAD), len * Math.sin(angle / DEG_TO_RAD));
    };
    /**
     * 从此点的坐标中减去另一个点的坐标以创建一个新点。
     * @param v 要减去的点。
     * @returns 新点。
     */
    Point.prototype.subtract = function (v) {
        return new Point(this.x - v.x, this.y - v.y);
    };
    /**
     * 返回包含 x 和 y 坐标的值的字符串。该字符串的格式为 "(x=x, y=y)"，因此为点 23,17 调用 toString() 方法将返回 "(x=23, y=17)"。
     * @returns 坐标的字符串表示形式。
     */
    Point.prototype.toString = function () {
        return "(x=" + this.x + ", y=" + this.y + ")";
    };
    /**
     * 返回包含 x 和 y 坐标值的数组
     */
    Point.prototype.toArray = function () {
        return [this.x, this.y];
    };
    Point.prototype.xchange = function (propertyKey, oldValue, newValue) {
        console.log("xchange", propertyKey, oldValue, newValue);
    };
    __decorate([
        watch("xchange")
    ], Point.prototype, "x", void 0);
    return Point;
}());

/**
 * Vector3D 类使用笛卡尔坐标 x、y 和 z 表示三维空间中的点或位置
 * @author feng 2016-3-21
 */
var Vector3D = /** @class */ (function () {
    /**
     * 创建 Vector3D 对象的实例。如果未指定构造函数的参数，则将使用元素 (0,0,0,0) 创建 Vector3D 对象。
     * @param x 第一个元素，例如 x 坐标。
     * @param y 第二个元素，例如 y 坐标。
     * @param z 第三个元素，例如 z 坐标。
     * @param w 表示额外数据的可选元素，例如旋转角度
     */
    function Vector3D(x, y, z, w) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (z === void 0) { z = 0; }
        if (w === void 0) { w = 0; }
        /**
        * Vector3D 对象中的第一个元素，例如，三维空间中某个点的 x 坐标。默认值为 0
        */
        this.x = 0;
        /**
         * Vector3D 对象中的第二个元素，例如，三维空间中某个点的 y 坐标。默认值为 0
         */
        this.y = 0;
        /**
         * Vector3D 对象中的第三个元素，例如，三维空间中某个点的 z 坐标。默认值为 0
         */
        this.z = 0;
        /**
         * Vector3D 对象的第四个元素（除了 x、y 和 z 属性之外）可以容纳数据，例如旋转角度。默认值为 0
         */
        this.w = 0;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    Vector3D.fromArray = function (array, offset) {
        if (offset === void 0) { offset = 0; }
        return new Vector3D().fromArray(array, offset);
    };
    Object.defineProperty(Vector3D.prototype, "length", {
        /**
        * 当前 Vector3D 对象的长度（大小），即从原点 (0,0,0) 到该对象的 x、y 和 z 坐标的距离。w 属性将被忽略。单位矢量具有的长度或大小为一。
        */
        get: function () {
            return Math.sqrt(this.lengthSquared);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3D.prototype, "lengthSquared", {
        /**
        * 当前 Vector3D 对象长度的平方，它是使用 x、y 和 z 属性计算出来的。w 属性将被忽略。尽可能使用 lengthSquared() 方法，而不要使用 Vector3D.length() 方法的 Math.sqrt() 方法调用，后者速度较慢。
        */
        get: function () {
            return this.x * this.x + this.y * this.y + this.z * this.z;
        },
        enumerable: true,
        configurable: true
    });
    Vector3D.prototype.fromArray = function (array, offset) {
        if (offset === void 0) { offset = 0; }
        this.x = array[offset];
        this.y = array[offset + 1];
        this.z = array[offset + 2];
        return this;
    };
    /**
     * 将当前 Vector3D 对象的 x、y 和 z 元素的值与另一个 Vector3D 对象的 x、y 和 z 元素的值相加。
     * @param a 要与当前 Vector3D 对象相加的 Vector3D 对象。
     * @return 一个 Vector3D 对象，它是将当前 Vector3D 对象与另一个 Vector3D 对象相加所产生的结果。
     */
    Vector3D.prototype.add = function (a) {
        return new Vector3D(this.x + a.x, this.y + a.y, this.z + a.z, this.w + a.w);
    };
    /**
     * 返回一个新 Vector3D 对象，它是与当前 Vector3D 对象完全相同的副本。
     * @return 一个新 Vector3D 对象，它是当前 Vector3D 对象的副本。
     */
    Vector3D.prototype.clone = function () {
        return new Vector3D(this.x, this.y, this.z, this.w);
    };
    /**
     * 将源 Vector3D 对象中的所有矢量数据复制到调用方 Vector3D 对象中。
     * @return 要从中复制数据的 Vector3D 对象。
     */
    Vector3D.prototype.copyFrom = function (sourceVector3D) {
        this.x = sourceVector3D.x;
        this.y = sourceVector3D.y;
        this.z = sourceVector3D.z;
        this.w = sourceVector3D.w;
    };
    /**
     * 返回一个新的 Vector3D 对象，它与当前 Vector3D 对象和另一个 Vector3D 对象垂直（成直角）。
     */
    Vector3D.prototype.crossProduct = function (a) {
        return new Vector3D(this.y * a.z - this.z * a.y, this.z * a.x - this.x * a.z, this.x * a.y - this.y * a.x, 1);
    };
    /**
     * 按照指定的 Vector3D 对象的 x、y 和 z 元素的值递减当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    Vector3D.prototype.decrementBy = function (a) {
        this.x -= a.x;
        this.y -= a.y;
        this.z -= a.z;
    };
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素乘以指定的 Vector3D 对象的 x、y 和 z 元素得到新对象。
     */
    Vector3D.prototype.multiply = function (a) {
        return new Vector3D(this.x * a.x, this.y * a.y, this.z * a.z);
    };
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素除以指定的 Vector3D 对象的 x、y 和 z 元素得到新对象。
     */
    Vector3D.prototype.divide = function (a) {
        return new Vector3D(this.x / a.x, this.y / a.y, this.z / a.z);
    };
    /**
     * 如果当前 Vector3D 对象和作为参数指定的 Vector3D 对象均为单位顶点，此方法将返回这两个顶点之间所成角的余弦值。
     */
    Vector3D.prototype.dotProduct = function (a) {
        return this.x * a.x + this.y * a.y + this.z * a.z;
    };
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素与指定的 Vector3D 对象的 x、y 和 z 元素进行比较，确定这两个对象是否相等。
     */
    Vector3D.prototype.equals = function (object, allFour, precision) {
        if (allFour === void 0) { allFour = false; }
        if (precision === void 0) { precision = 0.0001; }
        if (Math.abs(this.x - object.x) > precision)
            return false;
        if (Math.abs(this.y - object.y) > precision)
            return false;
        if (Math.abs(this.z - object.z) > precision)
            return false;
        if (allFour && Math.abs(this.w - object.w) > precision)
            return false;
        return true;
    };
    /**
     * 按照指定的 Vector3D 对象的 x、y 和 z 元素的值递增当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    Vector3D.prototype.incrementBy = function (a) {
        this.x += a.x;
        this.y += a.y;
        this.z += a.z;
    };
    /**
     * 将当前 Vector3D 对象设置为其逆对象。
     */
    Vector3D.prototype.negate = function () {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
    };
    /**
     * 通过将最前面的三个元素（x、y、z）除以矢量的长度可将 Vector3D 对象转换为单位矢量。
     */
    Vector3D.prototype.normalize = function (thickness) {
        if (thickness === void 0) { thickness = 1; }
        if (this.length != 0) {
            var invLength = thickness / this.length;
            this.x *= invLength;
            this.y *= invLength;
            this.z *= invLength;
            return;
        }
    };
    /**
     * 按标量（大小）缩放当前的 Vector3D 对象。
     */
    Vector3D.prototype.scaleBy = function (s) {
        this.x *= s;
        this.y *= s;
        this.z *= s;
        return this;
    };
    /**
     * 将 Vector3D 的成员设置为指定值
     */
    Vector3D.prototype.setTo = function (x, y, z, w) {
        this.x = x;
        this.y = y;
        this.z = z;
        if (w !== undefined)
            this.w = w;
        return this;
    };
    /**
     * 从另一个 Vector3D 对象的 x、y 和 z 元素的值中减去当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    Vector3D.prototype.subtract = function (a) {
        return new Vector3D(this.x - a.x, this.y - a.y, this.z - a.z);
    };
    /**
     * 返回当前 Vector3D 对象的字符串表示形式。
     */
    Vector3D.prototype.toString = function () {
        return "<" + this.x + ", " + this.y + ", " + this.z + ">";
    };
    /**
     * 返回当前 Vector3D 对象4个元素的数组
     */
    Vector3D.prototype.toArray = function (num) {
        if (num === void 0) { num = 4; }
        if (num == 3) {
            return [this.x, this.y, this.z];
        }
        else {
            return [this.x, this.y, this.z, this.w];
        }
    };
    /**
    * 定义为 Vector3D 对象的 x 轴，坐标为 (1,0,0)。
    */
    Vector3D.X_AXIS = new Vector3D(1, 0, 0);
    /**
    * 定义为 Vector3D 对象的 y 轴，坐标为 (0,1,0)
    */
    Vector3D.Y_AXIS = new Vector3D(0, 1, 0);
    /**
    * 定义为 Vector3D 对象的 z 轴，坐标为 (0,0,1)
    */
    Vector3D.Z_AXIS = new Vector3D(0, 0, 1);
    return Vector3D;
}());

console.log(new Point(1, 2));
console.log(new Vector3D(1, 2, 3.01, 5));

export { Point, Vector3D };
//# sourceMappingURL=index.es.js.map
