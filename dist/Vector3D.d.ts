/**
 * Vector3D 类使用笛卡尔坐标 x、y 和 z 表示三维空间中的点或位置
 * @author feng 2016-3-21
 */
export declare class Vector3D {
    /**
    * 定义为 Vector3D 对象的 x 轴，坐标为 (1,0,0)。
    */
    static X_AXIS: Vector3D;
    /**
    * 定义为 Vector3D 对象的 y 轴，坐标为 (0,1,0)
    */
    static Y_AXIS: Vector3D;
    /**
    * 定义为 Vector3D 对象的 z 轴，坐标为 (0,0,1)
    */
    static Z_AXIS: Vector3D;
    static fromArray(array: ArrayLike<number>, offset?: number): Vector3D;
    /**
    * Vector3D 对象中的第一个元素，例如，三维空间中某个点的 x 坐标。默认值为 0
    */
    x: number;
    /**
     * Vector3D 对象中的第二个元素，例如，三维空间中某个点的 y 坐标。默认值为 0
     */
    y: number;
    /**
     * Vector3D 对象中的第三个元素，例如，三维空间中某个点的 z 坐标。默认值为 0
     */
    z: number;
    /**
     * Vector3D 对象的第四个元素（除了 x、y 和 z 属性之外）可以容纳数据，例如旋转角度。默认值为 0
     */
    w: number;
    /**
    * 当前 Vector3D 对象的长度（大小），即从原点 (0,0,0) 到该对象的 x、y 和 z 坐标的距离。w 属性将被忽略。单位矢量具有的长度或大小为一。
    */
    readonly length: number;
    /**
    * 当前 Vector3D 对象长度的平方，它是使用 x、y 和 z 属性计算出来的。w 属性将被忽略。尽可能使用 lengthSquared() 方法，而不要使用 Vector3D.length() 方法的 Math.sqrt() 方法调用，后者速度较慢。
    */
    readonly lengthSquared: number;
    /**
     * 创建 Vector3D 对象的实例。如果未指定构造函数的参数，则将使用元素 (0,0,0,0) 创建 Vector3D 对象。
     * @param x 第一个元素，例如 x 坐标。
     * @param y 第二个元素，例如 y 坐标。
     * @param z 第三个元素，例如 z 坐标。
     * @param w 表示额外数据的可选元素，例如旋转角度
     */
    constructor(x?: number, y?: number, z?: number, w?: number);
    fromArray(array: ArrayLike<number>, offset?: number): this;
    /**
     * 将当前 Vector3D 对象的 x、y 和 z 元素的值与另一个 Vector3D 对象的 x、y 和 z 元素的值相加。
     * @param a 要与当前 Vector3D 对象相加的 Vector3D 对象。
     * @return 一个 Vector3D 对象，它是将当前 Vector3D 对象与另一个 Vector3D 对象相加所产生的结果。
     */
    add(a: Vector3D): Vector3D;
    /**
     * 返回一个新 Vector3D 对象，它是与当前 Vector3D 对象完全相同的副本。
     * @return 一个新 Vector3D 对象，它是当前 Vector3D 对象的副本。
     */
    clone(): Vector3D;
    /**
     * 将源 Vector3D 对象中的所有矢量数据复制到调用方 Vector3D 对象中。
     * @return 要从中复制数据的 Vector3D 对象。
     */
    copyFrom(sourceVector3D: Vector3D): void;
    /**
     * 返回一个新的 Vector3D 对象，它与当前 Vector3D 对象和另一个 Vector3D 对象垂直（成直角）。
     */
    crossProduct(a: Vector3D): Vector3D;
    /**
     * 按照指定的 Vector3D 对象的 x、y 和 z 元素的值递减当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    decrementBy(a: Vector3D): void;
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素乘以指定的 Vector3D 对象的 x、y 和 z 元素得到新对象。
     */
    multiply(a: Vector3D): Vector3D;
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素除以指定的 Vector3D 对象的 x、y 和 z 元素得到新对象。
     */
    divide(a: Vector3D): Vector3D;
    /**
     * 如果当前 Vector3D 对象和作为参数指定的 Vector3D 对象均为单位顶点，此方法将返回这两个顶点之间所成角的余弦值。
     */
    dotProduct(a: Vector3D): number;
    /**
     * 通过将当前 Vector3D 对象的 x、y 和 z 元素与指定的 Vector3D 对象的 x、y 和 z 元素进行比较，确定这两个对象是否相等。
     */
    equals(object: Vector3D, allFour?: boolean, precision?: number): boolean;
    /**
     * 按照指定的 Vector3D 对象的 x、y 和 z 元素的值递增当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    incrementBy(a: Vector3D): void;
    /**
     * 将当前 Vector3D 对象设置为其逆对象。
     */
    negate(): void;
    /**
     * 通过将最前面的三个元素（x、y、z）除以矢量的长度可将 Vector3D 对象转换为单位矢量。
     */
    normalize(thickness?: number): void;
    /**
     * 按标量（大小）缩放当前的 Vector3D 对象。
     */
    scaleBy(s: number): this;
    /**
     * 将 Vector3D 的成员设置为指定值
     */
    setTo(x: number, y: number, z: number, w?: number): this;
    /**
     * 从另一个 Vector3D 对象的 x、y 和 z 元素的值中减去当前 Vector3D 对象的 x、y 和 z 元素的值。
     */
    subtract(a: Vector3D): Vector3D;
    /**
     * 返回当前 Vector3D 对象的字符串表示形式。
     */
    toString(): string;
    /**
     * 返回当前 Vector3D 对象4个元素的数组
     */
    toArray(num?: 3 | 4): number[];
}
